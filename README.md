Ked to chcete nejak spojazdnit cez windows tak tough luck (predpoklada sa, ze vam budu fungovat linuxove prikazy a netestoval som to na windowse)

TLDR:
do Kontru som poslal subor test.txt ->
vysledky vsetkych vypoctov som ulozil do testresult.txt. ->
vy teraz cez python spustite testy v test.txt na vasom rieseni ->
po zbehnuti suboru sa zacnu porovnavat vase vysledky s vysledkami Kontru ->
funkcia printne prvu chybu ktoru najde (vystup ktory sa nezhoduje u vas a kontra) ->
nasledne si to opravite vo vasom .c ->
a takto dookola budete spustat python na vasom zadani az dokial vam neprejdu vsetky testy

Navod:

1. nemenit subor testresult.txt ani test.txt (nikdy)(vysledky, ktore vyplul KONTR)
2. otvorit python subor
3. dole je main funkcia a nad nou je konstanta YOUR_PATH
4. do YOUR_PATH chcete ulozit cestu k binarke vasho C programu (main bez .c)
5. spustite bruteForce.py, ktory vam vypise prvu chybu, ktoru najde
6. dostanete vstup, na ktorom sa vas vystup programu nezhoduje s tym z kontru - napriklad TTTMX
7. teraz vam odporucam si skopirovat tento vypocet a vlozit ho na aise do tej vzorovej implementacie pomocou:
   cat -en 'TTTMX' | /home/kontr/pb071/hw01/calc
8. to iste spravte lokalne na vasom zadani
9. porovnajte vysledky a upravte vas .c program tak aby bol vysledok spravny
10.   nezabudnite .c program skompilovat !
11.   opakujte krok 7. az dokial program nenajde ziadnu chybu

Spekulacia:

1. ked sa bude program dostavat po testy ktore su daleko v subore, tak mozu vypocty trvat celkom dlho (5-10 minut)
2. mozete si vtedy prepisat konstantu skip_passed na true, co urobi ze sa vam automaticky preskocia testy, ktore vam naposledy presli, treba si ale dat pozor lebo neni istota ak ste daco zmenili v cckovom subore ze budu tie predchadzajuce testy stale prechadzat
3. zaroven su tam aj 2 funkcie navyse, ktore mozete pouzit ak by ste chceli sami vygenerovat nejaku sadu testov pre Kontr

4. je tam moj random attempt na nejaky randomizovany vstup (vygeneruje to nejake random testy, ale su v nefunkcnom formate, proste je to na vas)

5. generovat 6 vstupov (TTTTTT) by uz asi bol overkill na aisu so please dont do that pokial to je viac ako 500 000 vypoctov

Troubleshooting:
:)
