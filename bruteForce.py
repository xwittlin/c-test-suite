import subprocess
import random
import os

ALPHABET = ['T', 'P', '100', '0', '-', '/', '>', 'M', 'm', '=', 'l', ';']


def get_result(path: str, input: str) -> str:
    p = subprocess.run(["echo " + '\''+input+'\'' + " | " + path],
                       stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                       shell=True, encoding="utf-8")
    return p.stdout+p.stderr


def generateInput(generateTo: str):
    count = 1
    with open(generateTo, 'w') as file:
        for a in ALPHABET:
            for b in ALPHABET:
                for c in ALPHABET:
                    for d in ALPHABET:
                        for e in ALPHABET:
                            if(a not in {'100', '0', 'm'}):
                                if(not ([a, b, c, d, e].count('/') + [a, b, c, d, e].count('-') +
                                        [a, b, c, d, e].count('>') + [a, b, c, d, e].count('l') +
                                        [a, b, c, d, e].count('P') > 3)):
                                    if(not a == b == c == d == e):
                                        if (not b == c == d):
                                            if(not c == d == e):
                                                if(not a == b == c):
                                                    file.write(
                                                        str(count)+'\n'+a+b+c+d+e+'\n')
                                                    count = count + 2


def feed_kontr(read: str, kontr: str, skip_passed: bool):

    readf = open(read, 'r')
    kontrf = open(kontr, 'r')
    startf = None
    start_from = 1

    if skip_passed:
        try:
            startf = open('start.txt', 'r')
            start_from = int(startf.readline().strip('\n'))
            startf.close()
        except:
            start_from = 1

        while kontrf.readline().strip('\n') != str(start_from):
            pass
    print(f"Starting tests from {start_from}")
    print()

    last_index = 0
    error = False
    for i, calculation in enumerate(readf):
        if calculation.strip().isnumeric():
            last_index = int(calculation.strip())
        if last_index < int(start_from) and skip_passed:
            continue
        if i % 1000 == 0:
            print(f"{i} calculations done")
        if(not calculation[0].isnumeric()):
            correct = ""
            result = get_result(YOUR_PATH, calculation)
            line = ""
            if start_from != last_index or not skip_passed:
                kontrf.readline()
            while line != '\n':
                line = kontrf.readline()
                correct += line
            correct = correct.strip()
            result = result.strip()
            if correct != result:
                print(f"Error in test {i}")
                print(f"Test: {calculation}")
                print(f"Expected:\n{correct}\n")
                print(f"Actual:\n{result}")
                error = True
                break
    readf.close()
    kontrf.close()

    if (not error):
        print("Congratulations! All tests passed!")

    with open('start.txt', 'w') as startf:
        startf.write(str(last_index))


# kontr pevna cesta
KONTR_PATH = "/home/kontr/pb071/hw01/calc"

# cesta k vasej binarke (treba zmenit)
YOUR_PATH = "/home/samuelw2/MUNI/sem2/PB071/repo/pb071/hw01/build/calc"

# ci chceme preskocit testy, ktore nam uz pred tym presli (da sa tym usetrit cas ale treba pamatat
# ze neni istota ak ste daco zmenili ze budu tie predchadzajuce testy stale prechadzat )
SKIP_PASSED = False


def main() -> None:
    # print(get_result(YOUR_PATH, 'P192;'))
    # generateInput("/home/xvalko3/test.txt")

    feed_kontr(f"{os.getcwd()}/test.txt",
               f"{os.getcwd()}/testresult.txt",
               SKIP_PASSED)


if __name__ == '__main__':
    main()
